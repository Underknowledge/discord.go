# Discord.go

This Go application sends output to a Discord channel via webhooks, allowing users to monitor very lazy system status.
It can be also used in a pipe making it useful for scripts.
Based on an older python implementation, rewritten with a sometimes unwilling GPT3.5.
Discords documentation on webhooks can be found [here](https://discord.com/developers/docs/resources/webhook#execute-webhook)

the script runs by default these commands ( wrapped in a 'bash -c' ), and sends the output to a discord webhook
```bash
	w
	free -h
	timeout 5 df -H | grep -v 'Filesystem\\|tmpfs\\|cdrom\\|loop\\|overlay'
	curl -4 ifconfig.co/json
	curl -6 ifconfig.co/json
	dmesg -T | grep -i 'error\\|warn'
	systemctl | grep -i error
	docker ps
	hostname -I
```


## Usage

To quickly install the bin what fell out of CI you can run this block 
```bash
command -v unzip > /dev/null 2>&1 || (echo "Install unzip first" ; exit )
curl -sSL "https://gitlab.com/Underknowledge/discord.go/-/jobs/artifacts/main/download?job=build" -o /tmp/discord.go.zip
mkdir -p /tmp/artifacts
unzip -j /tmp/discord.go.zip -d /tmp/artifacts
pushd /tmp/
md5sum --check /tmp/artifacts/discord_$(uname -m).go && rm -f /tmp/discord.go.zip && 
mv /tmp/artifacts/discord_$(uname -m).go /usr/local/bin/discord.go && rm -rf /tmp/artifacts
popd
```
then `webhook_url= discord.go` or `webhook_url= discord.go ` 


```bash
# Set your Discord webhook URL as an environment variable
export webhook_url="YOUR_DISCORD_WEBHOOK_URL"

# send the output of an ls
discord.go --command 'ls'

# or 
DEBUG=true webhook_url=https://discord.com/api//webhooks/${webhook.id}/${webhook.token} discord.go --username TEST3 --append-command "ls" --append-command "ls -lah"
```

### Custom Commands

You can replace the default commands using the `--commands flag:

```bash
discord.go --command "ls" --command "du"
```

When you like the default commands, you can add custom commands using the `--append-command` flag. For example:

```bash
discord.go --append-command "ls" --append-command "du"
```

## Installation as a Systemd Service

To install the application as a systemd service and timer, use the `--install-service` flag:    
This command will configure a service and will use the currently configured token (the application may fail without one)     
`WorkingDirectory` will be set as the curreltly used working directory.    

```bash
webhook_url=https://disco... discord.go --install-service
```
Will abort when youre not using systemd. 
The systemd unit file will be created at `/etc/systemd/system/discord-monitoring.service`.     
It assumes that everything is set up correctly if the file already exists.    
When you want to configure `--command` or any other flag run `systemctl edit --full discord-monitoring.service`    
Same for the `.timer` service. 

After installation, the service will automatically send a message to your channel every 8 hours to provide updates.

<!-- 
when I feel veeeery motivated, implement a --user service with checking of lingering
-->


## Building 

check the .gitlab-ci.yml or 
```bash
rm -f discord.go ; CGO_ENABLED=0 go build -o discord.go
```