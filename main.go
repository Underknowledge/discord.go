package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"os/exec"
	"strings"
	"time"
)

type DiscordMessage struct {
	Content   string `json:"content"`
	Username  string `json:"username"`
	AvatarURL string `json:"avatar_url"`
}
type DiscordEmbed struct {
	Username    string         `json:"username"`
	AvatarURL   string         `json:"avatar_url"`
	Title       string         `json:"title"`
	Color       int            `json:"color"`
	Description string         `json:"description"`
	Time        string         `json:"timestamp"`
	Fields      []EmbedField   `json:"fields"`
	Author      *DiscordAuthor `json:"author,omitempty"`
	Footer      struct {
		Text string `json:"text"`
	} `json:"footer"`
}
type DiscordAuthor struct {
	Name     string `json:"name,omitempty"`
	URL      string `json:"url,omitempty"`
	IconURL  string `json:"icon_url,omitempty"`
	ProxyURL string `json:"proxy_icon_url,omitempty"`
}

// see
// https://birdie0.github.io/discord-webhooks-guide/structure/embed/fields.html
// https://discord.com/developers/docs/resources/webhook#execute-webhook
type EmbedField struct {
	Name   string `json:"name"`
	Value  string `json:"value"`
	Inline bool   `json:"inline"`
}

var randomColor int
var hostname string
var avatarURL string

type CommandList []string

var defaultCommands = []string{
	"w",
	"free -h",
	"timeout 5 df -H | grep -v 'Filesystem\\|tmpfs\\|cdrom\\|loop\\|overlay'",
	"curl -4 ifconfig.co/json",
	"curl -6 ifconfig.co/json",
	"dmesg -T | grep -i 'error\\|warn'",
	"systemctl | grep -i error",
	"docker ps",
	"hostname -I",
}

func (cl *CommandList) String() string {
	return fmt.Sprint(*cl)
}

func (cl *CommandList) Set(value string) error {
	*cl = append(*cl, value)
	return nil
}

func init() {
	randomColor = generateRandomDecColor()
	var err error
	hostname, err = os.Hostname()
	if err != nil {
		fmt.Println("Error fetching hostname:", err)
	}
}

func main() {

	// Parse the username and avatar_url flags
	var (
		usernameFlag       string
		avatarURLFlag      string
		showHelp           bool
		installServiceFlag bool
		// custom commands slice
		commands          CommandList
		appendCommands    CommandList
		overwriteCommands CommandList
	)
	flag.StringVar(&usernameFlag, "username", "", "Override Discord's username with this flag")
	flag.StringVar(&avatarURLFlag, "avatar_url", "", "Override Discord's default avatar URL with this flag")
	flag.BoolVar(&showHelp, "help", false, "Show help message")
	flag.BoolVar(&installServiceFlag, "install-service", false, "Install systemd service and timer discord-monitoring.service, using the default commands")
	flag.Var(&appendCommands, "append-command", "Add a command to the list of commands (can be defined multiple times)")
	flag.Var(&overwriteCommands, "command", "Overwrite the list of commands (can be defined multiple times)")

	flag.Parse()

	if showHelp {
		flag.Usage()
		displayHelp(defaultCommands)
		return
	}

	webhookURL := os.Getenv("webhook_url")
	if webhookURL == "" {
		fmt.Println("Error: Webhook URL not defined in the environment variable 'webhook_url'.")
		return
	}

	if installServiceFlag {
		// Install systemd service and timer
		installSystemdService(webhookURL)
		return
	}

	// Check if DEBUG environment variable is set
	debugMode := os.Getenv("DEBUG") == "true"

	// Check if the application is used in a pipe (stdin has data)
	if isInputFromPipe() {
		// Read input from stdin and send it to Discord
		scanner := bufio.NewScanner(os.Stdin)
		var inputLines []string
		for scanner.Scan() {
			inputLines = append(inputLines, scanner.Text())
		}
		inputText := strings.Join(inputLines, "\n")

		// Determine the username and avatar_url to use
		username := getUsername(usernameFlag)
		avatarURL = getAvatarURL(avatarURLFlag)

		// Send the stdin input to Discord, splitting into chunks if necessary
		sendLongMessageToDiscordWithRetry(webhookURL, username, avatarURL, "", inputText, debugMode)
	} else {
		// Define the list of commands to execute
		// Use appendCommands and overwriteCommands if provided
		if len(overwriteCommands) > 0 {
			commands = overwriteCommands
		} else {
			// Default commands
			commands = defaultCommands
		}

		// Append commands provided through the appendCommands flag
		commands = append(commands, appendCommands...)

		// Iterate over each command, execute it, and send the output to Discord
		for _, command := range commands {
			// Determine the username and avatar_url to use
			username := getUsername(usernameFlag)
			avatarURL = getAvatarURL(avatarURLFlag)

			sendEmbedToDiscord(webhookURL, username, avatarURL, command, debugMode)
			output, err := executeCommand(command)
			if err != nil {
				fmt.Printf("Error executing command '%s': %v\n", command, err)
				continue
			}

			// Send the command output to Discord, splitting into chunks if necessary
			sendLongMessageToDiscordWithRetry(webhookURL, username, avatarURL, command, output, debugMode)
		}
	}
}

func displayHelp(commands []string) {
	fmt.Println(`

This Go application sends command outputs to a Discord Webhook.
Can be used in a pipe
It supportsto define and execute commands,
making it a  lazy tool for system monitoring and reporting.
Without arguments, it sends the output of these commands`)
	for _, command := range commands {
		fmt.Println("    ", command)
	}
	fmt.Println(`use --command to replace or --append-command to add commands to this list
it is needed to set the URL to the webhook in the env var 'webhook_url'`)
}

func generateRandomDecColor() int {
	rand.Seed(time.Now().UnixNano())
	r := rand.Intn(256)
	g := rand.Intn(256)
	b := rand.Intn(256)
	color := (r << 16) + (g << 8) + b
	return color
}

// func sendEmbedToDiscord(webhookURL, command, username string, debugMode bool) error {
func sendEmbedToDiscord(webhookURL, username, avatarURL, command string, debugMode bool) error {

	currentTime := time.Now().UTC()
	iso8601Timestamp := currentTime.Format("2006-01-02T15:04:05Z")

	embedData := DiscordEmbed{
		// Title: hostname, // replaced by Author
		Username: username,
		Color:    randomColor,
		Time:     iso8601Timestamp,
		Fields: []EmbedField{
			{
				Name:   "Comand:",
				Value:  command,
				Inline: true,
			},
		},
		Footer: struct {
			Text string `json:"text"`
		}{
			Text: "Run at: ",
		},
		Author: &DiscordAuthor{
			Name: hostname,
			// URL:      "https://example.com", // Could add the URL as a flag maybe, --urltoservice or --embed idk yet
			IconURL: avatarURL,
			// ProxyURL: "https://proxy.example.com/icon.png", / from docu, why would I?
		},
	}

	payloadData := struct {
		Username string         `json:"username"`
		Embeds   []DiscordEmbed `json:"embeds"`
	}{
		Username: username,
		Embeds:   []DiscordEmbed{embedData},
	}

	payload, err := json.Marshal(payloadData)
	if err != nil {
		return err
	}

	if debugMode {
		fmt.Printf("DEBUG: JSON Payload to Discord for embed command '%s': %s\n", command, string(payload))
	}

	resp, err := http.Post(webhookURL, "application/json", bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("error sending embed to Discord for command '%s'. Status code: %d", command, resp.StatusCode)
	}

	fmt.Printf("Embed sent to Discord successfully for command '%s'.\n", command)
	return nil
}

func isInputFromPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return (fileInfo.Mode() & os.ModeCharDevice) == 0
}

func executeCommand(command string) (string, error) {
	cmd := exec.Command("bash", "-c", command)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return "", err
	}
	return string(output), nil
}

func sendToDiscord(webhookURL, username, avatarURL, message, command string, debugMode bool) error {
	payloadData := DiscordMessage{
		Content:   message,
		Username:  username,
		AvatarURL: avatarURL,
	}
	payload, err := json.Marshal(payloadData)
	if err != nil {
		return err
	}

	if debugMode {
		fmt.Printf("DEBUG: JSON Payload to Discord for command '%s': %s\n", command, string(payload))
	}

	resp, err := http.Post(webhookURL, "application/json", bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusNoContent {
		// return fmt.Errorf("Error sending message to Discord for command '%s'. Status code: %d", command, resp.StatusCode)
		return fmt.Errorf("error sending message to Discord for command '%s'. Status code: %d", command, resp.StatusCode)

	}

	fmt.Printf("Message sent to Discord successfully for command '%s'.\n", command)
	return nil
}

func sendLongMessageToDiscordWithRetry(webhookURL, username, avatarURL, command, message string, debugMode bool) {
	maxRetries := 3
	for i := 0; i < maxRetries; i++ {
		err := sendLongMessageToDiscord(webhookURL, username, avatarURL, command, message, debugMode)
		if err == nil {
			break
		} else {
			fmt.Printf("Error sending message for command '%s' (attempt %d): %v\n", command, i+1, err)
			time.Sleep(5 * time.Second) // Wait for a few seconds before retrying
		}
	}
}

func sendLongMessageToDiscord(webhookURL, username, avatarURL, command, message string, debugMode bool) error {
	maxLength := 2000
	if len(message) <= maxLength {
		return sendToDiscord(webhookURL, username, avatarURL, "```"+command+"\n"+message+"```", command, debugMode)
	}

	// Split the long message into chunks of max length
	chunks := splitMessage(message, maxLength)

	// Send each chunk as a separate message
	for _, chunk := range chunks {
		err := sendToDiscord(webhookURL, username, avatarURL, "```"+command+"\n"+chunk+"```", command, debugMode)
		if err != nil {
			return err
		}
	}

	return nil
}

func splitMessage(message string, maxLength int) []string {
	var chunks []string
	lines := strings.Split(message, "\n")
	currentChunk := ""

	for _, line := range lines {
		if len(currentChunk)+len(line) > maxLength {
			// If adding the line exceeds the max length, start a new chunk
			chunks = append(chunks, currentChunk)
			currentChunk = ""
		}
		currentChunk += line + "\n"
	}

	if currentChunk != "" {
		chunks = append(chunks, currentChunk)
	}

	return chunks
}

func getUsername(usernameFlag string) string {
	if usernameFlag != "" {
		return usernameFlag // Use the provided username from the flag
	}
	hostname, err := os.Hostname()
	if err != nil {
		fmt.Printf("Error getting hostname: %v\n", err)
		return "Unknown"
	}
	return hostname
}

func getAvatarURL(avatarURLFlag string) string {
	return avatarURLFlag // Use the provided avatar URL from the flag
}

func isSystemdSystem() bool {
	_, err := os.Stat("/run/systemd/system")
	return err == nil
}

// // Keeping here for now if other implementation turns out terribe
// func isSystemdSystem() bool {
// 	// Check if systemctl command exists or if /etc/systemd directory exists
// 	_, err := exec.LookPath("systemctl")
// 	if err == nil {
// 		return true
// 	}

// 	_, dirErr := os.Stat("/etc/systemd")
// 	if dirErr == nil {
// 		return true
// 	}

// 	return false
// }

func installSystemdService(webhookURL string) {

	executablePath, err := os.Executable()
	if err != nil {
		fmt.Printf("Error getting executable path: %v\n", err)
		return
	}

	// Check if the system is using systemd
	if !isSystemdSystem() {
		fmt.Println("Systemd is not in use on this system. Service installation skipped.")
		fmt.Println("chron is another option")
		fmt.Printf("@reboot (/bin/sleep 120 && /usr/bin/env webhook_url=https://discord.com/api/webhooks/${webhook.id}/${webhook.token} %s --command 'echo \"BOOTED\"' --command 'df -h' --command 'curl -4 ifconfig.co/json' --command 'uptime') >> /tmp/discord_cron.log 2>&1\n", executablePath)
		fmt.Printf("0 */8 * * * (/usr/bin/env webhook_url=https://discord.com/api/webhooks/${webhook.id}/${webhook.token} %s --command 'df -h' --command 'curl -4 ifconfig.co/json' --command 'uptime') >> /tmp/discord_cron.log 2>&1\n", executablePath)
		return
	}

	workingDirectory, wdErr := os.Getwd()
	if wdErr != nil {
		fmt.Printf("Error getting current working directory: %v\n", wdErr)
		return
	}

	servicepath := "/etc/systemd/system/discord-monitoring.service"
	servicefile := fmt.Sprintf(`[Unit]
Description=Go Discord Monitoring Service
Wants=network-online.target
After=network-online.target

[Service]
ExecStart=%s
WorkingDirectory=%s
Environment=webhook_url=%s
Type=oneshot

[Install]
WantedBy=multi-user.target`, executablePath, workingDirectory, webhookURL)

	timerpath := "/etc/systemd/system/discord-monitoring.timer"
	timerfile := `[Unit]
Description=Run Discord Monitoring Service

[Timer]
OnBootSec=1min
OnUnitActiveSec=8h

[Install]
WantedBy=timers.target`

	_, err = os.Stat(servicepath)
	if err != nil && os.IsNotExist(err) {
		f, err := os.Create(servicepath)
		if err != nil {
			fmt.Printf("Error creating service file: %v\n", err)
			return
		}
		defer f.Close()

		_, err = f.WriteString(servicefile)
		if err != nil {
			fmt.Printf("Error writing service file: %v\n", err)
			return
		}
	}

	_, err = os.Stat(timerpath)
	if err != nil && os.IsNotExist(err) {
		f, err := os.Create(timerpath)
		if err != nil {
			fmt.Printf("Error creating timer file: %v\n", err)
			return
		}
		defer f.Close()

		_, err = f.WriteString(timerfile)
		if err != nil {
			fmt.Printf("Error writing timer file: %v\n", err)
			return
		}

		// Reload systemd, enable and start the service and timer
		reloadCmd := exec.Command("systemctl", "daemon-reload")
		enableServiceCmd := exec.Command("systemctl", "enable", "--now", "discord-monitoring.service")
		enableTimerCmd := exec.Command("systemctl", "enable", "--now", "discord-monitoring.timer")

		if err := reloadCmd.Run(); err != nil {
			fmt.Printf("Error reloading systemd: %v\n", err)
			return
		}

		if err := enableServiceCmd.Run(); err != nil {
			fmt.Printf("Error enabling and starting service: %v\n", err)
			return
		}

		if err := enableTimerCmd.Run(); err != nil {
			fmt.Printf("Error enabling and starting timer: %v\n", err)
			return
		}

		fmt.Println("Systemd service and timer installed and started successfully.")
	}
}
